package com.thanawuth.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank worawit = new BookBank("Worawit", 100);
        worawit.print();
        worawit.desposit(50);
        worawit.print();
        worawit.withdraw(50);
        worawit.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.desposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();
        
        BookBank praweet = new BookBank("Praweet",10);
        praweet.desposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();

    }
}
