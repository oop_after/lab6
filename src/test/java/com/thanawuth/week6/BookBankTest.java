package com.thanawuth.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    
    @Test
    public void shouldDespositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.desposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldDespositNagative() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.desposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);

    }

    
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.desposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldWithdrawNegative() {
        BookBank book = new BookBank("Name", 0);
        book.desposit(100);
        boolean result = book.desposit(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);

    }
    
    @Test
    public void shouldWithdrawOverBalnce() {
        BookBank book = new BookBank("Name", 0);
        book.desposit(50);
        boolean result = book.withdraw(1000);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    
    @Test
    public void shouldWithdraw100Balance100() {
        BookBank book = new BookBank("Name", 0);
        book.desposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

}
