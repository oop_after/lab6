package com.thanawuth.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int X_MAX = 19;
    public final static int Y_MIN = 0;
    public final static int Y_MAX = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;

    }

    public void print() {
        System.out.println(name + " x:"+ x +" y:"+ y);
    }

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean up() {
        if(y == Y_MIN) return false;
        y = y-1;
        return true;
    }

    public boolean down() {
        if(y==Y_MAX) return false;
        y=y+1;
        return true;
    }

    public boolean left() {
        x = x-1;
        return true;
    }

    public boolean right() {
        x = x+1;
        return true;
    }
}
