package com.thanawuth.week6;

public class BookBank {
    String name;
    double balance;
    BookBank(String name, double balance) {
        this.name = name;
        this.balance = balance;

    }

    boolean desposit(double money) {
        if (money <= 0) {
            return false;

        }
        balance = balance + money;
        return true;
    }

    boolean withdraw(double money) {
        if (money <= 0) {
            return false;
        }
        if (money > balance) {
            return false;
        }
        balance = balance - money;
        return true;

    }

    void print() {
        System.out.println(name + " " + balance);

    }
}
